# Carousel challenge 

### Develop and Build

You can use the below command to run the project in development mode(port 8080):
```bash
npm start
```
For building:
```bash
npm run build
```
Also, there is a command that builds and serves the project(port 8000):
```bash 
npm run build-serve
```

### Run Tests 
please use follow command to run tests:

```bash
npm test
```
Also for E2E test you can run:
```bash 
npm run cypress:open
```
### Notice
There could be many improvements and I hope we would discuss them further.
I have created a boilerplate for vanilla js and ts and you can find it [here](https://github.com/alireza-mh/Vanilla-js-ts-boilerplate)

Unfortunately, I didn't find time to use image API to search images and populate them to the carousels.

You can add image link or links (separate via `,`)to populate images to the carousel.
please include `http or https`(some of the improvement that can be handled by code).
every time you add an image a new one will append to the carousel.
It took a little more time than my estimation. :)
